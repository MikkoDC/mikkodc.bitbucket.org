var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var notify = require("gulp-notify");
var watch = require('gulp-watch');
var livereload = require('gulp-livereload');
var lr = require('tiny-lr');
var server = lr();
var util = require('gulp-util');

gulp.task('images', function(){
  gulp.src('library/src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('library/dist/images/'));
});

gulp.task('styles', function(){
  gulp.src(['library/src/sass/**/*.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest('library/dist/styles/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('library/dist/styles/'))
    .pipe(livereload())
    .pipe(notify("Compiling CSS Finished Master!"))
});

gulp.task('scripts', function(){
  return gulp.src('library/src/js/**/*.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('library/dist/js/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('library/dist/js/'))
    .pipe(livereload())
    .pipe(notify("Compiling JS Finished Master!"))
});

gulp.task('default', function(){
    livereload.listen();
    gulp.watch("library/src/sass/**/*.scss", ['styles']);
    gulp.watch("library/src/js/**/*.js", ['scripts']);
    gulp.watch('**/*.php').on('change', function(file) {
        server.changed(file.path);
        util.log(util.colors.yellow('PHP file changed' + ' (' + file.path + ')'));
    });
    gulp.watch('**/*.html').on('change', function(file) {
        server.changed(file.path);
        util.log(util.colors.yellow('HTML file changed' + ' (' + file.path + ')'));
    });
    gulp.watch('**/*.html', livereload.reload);
});
